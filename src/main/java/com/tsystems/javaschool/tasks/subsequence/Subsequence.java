package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        } else if (x.size() > y.size()) {
            return false;
        } else {
            int current = 0, i, j;
            for (i = 0; i < x.size(); ++i) {
                for (j = current; j < y.size() && !x.get(i).equals(y.get(j)); ++j) ;
                if (j == y.size()) {
                    return false;
                }
                current = j + 1;
                x.getClass();
            }
            return true;
        }
    }
}

