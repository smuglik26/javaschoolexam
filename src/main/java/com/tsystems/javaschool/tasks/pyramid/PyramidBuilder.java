package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null) || inputNumbers.size() >= Integer.MAX_VALUE - 1) {
            throw new CannotBuildPyramidException();
        }
        else if (getHightPyramid(inputNumbers.size())==0) {
            throw new CannotBuildPyramidException();
        } else {
            Collections.sort(inputNumbers);
            int rows = getHightPyramid(inputNumbers.size());
            int cols = getHightPyramid(inputNumbers.size()) * 2 - 1;
            int [][] resultArray = new int [rows][cols];
            resultArray[0][rows - 1] = inputNumbers.get(0);
            int n = 1; //индекс в inputNumbers
            for (int i = 1; i <= (rows - 1); i++) {
                for (int j = 0; j <= i; j++) {
                    resultArray[i][(rows -1) - i + 2 * j] = inputNumbers.get(n);
                    n++;
                }
            }
            return resultArray;
        }
    }

    public int getHightPyramid (int stringSize) {
        if (stringSize < 3){    //для случая, когда эл-ов меньше 3х
            return 0;
        }
        //для случая, когда эл-ов больше 3х
        int rows = 2, count = 1;
        while (count < stringSize){
            count += rows;
            rows++;
        }
        if (count == stringSize){
            return rows - 1;
        } else {
            return 0;
        }
    }
}
