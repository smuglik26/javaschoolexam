package com.tsystems.javaschool.tasks.calculator;

public class CheckMathOperators {
    public boolean checkOperators(String statement){
        if (statement == null || statement.contains(",") ||
                        statement.isEmpty() ||  statement.contains("..") ||
                        statement.contains("++") || statement.contains("--") ||
                        statement.contains("//") || statement.contains("**")){
            return true;
        }
        int countBrace = 0;
        for (char sym : statement.toCharArray()){
            if (sym == '('){
                countBrace++;
            }
            else if(sym == ')'){
                countBrace--;
            }
            if (countBrace != 0){
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

}
