package com.tsystems.javaschool.tasks.calculator;

public class RoundNumbers {
    private double result;

    public void round(double number){
        number *= 10000;
        long tempNumber = Math.round(number);
        this.result = (double) tempNumber / 10000;
    }

    public double getResult() {
        return result;
    }

}