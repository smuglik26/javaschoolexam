package com.tsystems.javaschool.tasks.calculator;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.Locale;

public  class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) throws ArithmeticException {
        CheckMathOperators check = new CheckMathOperators();
        RoundNumbers roundNumb = new RoundNumbers();
        UICalc calc = new UICalc();
        LinkedList<Double> numbers = new LinkedList<>();
        LinkedList<Character> mathOperators = new LinkedList<>();
        if (check.checkOperators(statement)){
            return null;
        }
        try {
            for (int i = 0; i < statement.length(); i++) {
                char sym = statement.charAt(i);
                if (sym == '(') {
                    mathOperators.add(sym);
                } else if (sym == ')') {
                    while (mathOperators.getLast() != '(') {
                        calc.UICalc(numbers, mathOperators.removeLast());
                        numbers.add(calc.getResult());
                    }
                    mathOperators.removeLast();
                } else if (sym == '+' || sym == '-' || sym == '/' || sym == '*') {
                    if (!mathOperators.isEmpty() && whoFirst(mathOperators.getLast()) >= whoFirst(sym)) {
                        calc.UICalc(numbers, mathOperators.removeLast());
                        numbers.add(calc.getResult());
                    }
                    mathOperators.add(sym);
                } else {
                    String s = "";
                    while (i < statement.length() && (Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.')) {
                        s += statement.charAt(i++);
                    }
                    --i;
                    numbers.add(Double.parseDouble(s));
                }
            }
            while (!mathOperators.isEmpty()) {
                calc.UICalc(numbers, mathOperators.removeLast());
                numbers.add(calc.getResult());
            }
        } catch (ArithmeticException e){
            return null;
        }
        double result = numbers.getLast();
        if (result % 1 == 0) {
            return format(result);
        } else {
            roundNumb.round(result);
            return format(roundNumb.getResult());
        }
    }

    private int whoFirst(char c){
        if (c == '*' || c == '/'){
            return 1;
        }
        else if (c == '+' || c == '-'){
            return 0;
        }
        else{
            return -1;
        }
    }
    private String format(double d){
        if(d == (long) d)
            return String.format(Locale.US, "%d", (long) d);
        else
            return String.format(Locale.US, "%s", d);
    }
}